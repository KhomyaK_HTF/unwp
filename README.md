# RU

С помощью этой программы вы можете подгружать и автоматически менять обои с сайта unsplash.com. Чтобы скомпилировать и установить эту программу, достаточно ввести в терминал следущие команды:
```shell
git clone https://gitea.it/KhomyaK_HTF/unwp
cd unwp
cargo b --release
sudo cp target/release/unwp /usr/bin/
```

# EN

With this program you can download and automatically change the wallpaper from unsplash.com. To compile and install this program, just type the following commands into the terminal:
```shell
git clone https://gitea.it/KhomyaK_HTF/unwp
cd unwp
cargo b --release
sudo cp target/release/unwp /usr/bin/
```