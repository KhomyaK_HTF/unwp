mod setwp;
use std::fs;

fn main() {
    for _ in 0..24 {
        setwp::del();
        let _ = fs::create_dir_all("/tmp/unwp").expect("Программе не удалось создать временный каталог для её работы.");
        let mut base_url = "https://source.unsplash.com/random/".to_string();
        let irl: &mut String = &mut base_url;

        for arg in std::env::args() {
            if arg == "hd" {
                irl.push_str("1280x720");
                setwp::download(irl);
            } else if arg == "fhd" {
                irl.push_str("1920x1080");
                setwp::download(irl);
            } else if arg == "qhd" {
                irl.push_str("3840x2160");
                setwp::download(irl);
            } else if arg == "uhd" {
                irl.push_str("7680x4320");
                setwp::download(irl);
            }
        }
    }
}

