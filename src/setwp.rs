use std::{thread::sleep, fs, time::{Duration, Instant}};
use download_rs::async_download::Download;
use wallpaper;

pub static FILENAME: &str = "/tmp/unwp/bg.png";

pub fn download(url: &mut String) {
    url.push_str("/?background/");

    let d = Download::new(&url, Some(FILENAME),None); match d.download() {
        Ok(_) => { println!("[ OK ]"); set() },
        Err(_e) => { println!("[ !! ]"); loop {
                match d.download() {
                    Ok(_) => { println!("[ OK ]"); break; },
                    Err(_e) => {
                        let _ = Instant::now();
                        sleep(Duration::new(5, 5));
                        println!("[ !! ]");
                        continue;
                    },
                };
            }
        },
    };
}

pub fn set() {
    let _ = wallpaper::set_from_path(FILENAME);
    let _ = wallpaper::set_mode(wallpaper::Mode::Fit);
    let _ = Instant::now(); sleep(Duration::new(3600, 3600));
    del()
}

pub fn del() {
    let _ = fs::remove_file(FILENAME);
}
